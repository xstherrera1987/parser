public class ParseTableException extends Exception {
	public ParseTableException(String message) {
		super(message);
	}
}
