public class PredictSetException extends Exception {
	public PredictSetException(String message) {
		super(message);
	}
}
