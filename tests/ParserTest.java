import static org.junit.Assert.*;
import org.junit.*;

import java.io.*;

public class ParserTest {
	@Test public void testParse() {
		String filename = "res/TestSource.lang";
		File file = new File(filename);
		Reader r = null;
		try {
			r = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			fail("Could not find file: " + filename);
		}
		
		String expected = TestData.getTestParseOutput();
		Parser p = new Parser();
		p.parseTable = TestData.getTestParseTable();
		p.productions = TestData.getTestProductions();
		p.ntLookup = TestData.getTestNonTerminalLookup();
		p.tkLookup = TestData.getTestTokenLookup();
		
		Writer w = new StringWriter();
		try {
			p.parse(r,w);
		} catch (ParserException e ) {
			fail("Parse Error.");
		} catch (IOException e) {
			fail("IO error");
		}
		
		assertEquals(expected, w.toString() );
	}
}
