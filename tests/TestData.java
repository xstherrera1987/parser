import java.util.*;

public class TestData {
	static String[] getTestColumnHeader() {
		return new String[] {
			"", "+", "-", "*", "/", "(", ")", "^", "num", "$"
		};
	}
	
	static String[] getTestRowHeader() {
		return new String[] {
			"", "E", "E’", "T", "T’", "P", "P’", "F"
		};
	}
	
	static String[][] getTestParseTableRaw() {
		return new String[][] {
				{"","+","-","*","/","(",")","^","num","$"},
				{"E","err","err","err","err","1","err","err","1","err"},
				{"E’","2","3","err","err","err","4","err","err","4"},
				{"T","err","err","err","err","5","err","err","5","err"},
				{"T’","8","8","6","7","err","8","err","err","8"},
				{"P","err","err","err","err","9","err","err","9","err"},
				{"P’","11","11","11","11","err","11","10","err","11"},
				{"F","err","err","err","err","12","err","err","13","err"}
		};
	}
	
	static String[][] getTestPredictSet() {
		return new String[][] {
				{"production id#","production","Predict Set"},
				{"1","E -> T E’","( num"},
				{"2","E’ -> + T E’","+"},
				{"3","E’ -> - T E’","-"},
				{"4","E’ ->  𝞴","$ )"},
				{"5","T -> P T’","( num"},
				{"6","T’ -> * P T’ ","*"},
				{"7","T’ -> / P T’","/"},
				{"8","T’ -> 𝞴","$ + - )"},
				{"9","P -> F P’","( num"},
				{"10","P’ -> ^ F P’","^"},
				{"11","P’ -> 𝞴","$ * / + - )"},
				{"12","F -> ( E )","("},
				{"13","F -> num","num"}
		};
	}

	static Map<String, NonTerminal> getTestNonTerminalLookup() {
		Map<String, NonTerminal> retVal = new HashMap<String, NonTerminal>();
		for ( NonTerminal n: getTestNonTerminals() )
			retVal.put(n.symbol, n);
		
		return retVal;
	}
	
	static Map<String, Token> getTestTokenLookup() {
		Map<String, Token> retVal = new HashMap<String, Token>();
		for (Token t: getTestTokens() ) {
			retVal.put(t.symbol, t);
		}
		
		return retVal;
	}
	
	static NonTerminal[] getTestNonTerminals() {
		NonTerminal[] retVal = new NonTerminal[8];
		
		retVal[0] = NonTerminal.NULL_NONTERMINAL;
		for (int i=1; i<8; i++) {
			retVal[i] = new NonTerminal();
			retVal[i].id = i;
		}
		
		retVal[1].symbol = "E";
		retVal[2].symbol = "E’";
		retVal[3].symbol = "T";
		retVal[4].symbol = "T’";
		retVal[5].symbol = "P";
		retVal[6].symbol = "P’";
		retVal[7].symbol = "F";
		
		return retVal;
	}
	
	static Token[] getTestTokens() {
		Token[] retVal = new Token[10];
		
		retVal[0] = Token.NULL_TOKEN;
		for (int i=1; i<10; i++) {
			retVal[i] = new Token();
			retVal[i].id = i;
		}

		retVal[1].symbol = "+";
		retVal[2].symbol = "-";
		retVal[3].symbol = "*";
		retVal[4].symbol = "/";
		retVal[5].symbol = "(";
		retVal[6].symbol = ")";
		retVal[7].symbol = "^";
		retVal[8].symbol = "num";
		retVal[9].symbol = "$";
		
		return retVal;
	}

	static Production[] getTestProductions() {
		Map<String, NonTerminal> ntLookup = getTestNonTerminalLookup();
		Map<String, Token> tkLookup = getTestTokenLookup();
		
		Production[] testVals = new Production[14];
		for (int i=0; i<14; i++) {
			testVals[i] = new Production();
			testVals[i].id = i;
		}
		
		testVals[0].lhs = new NonTerminal();
		testVals[0].lhs.id = 0;
		testVals[0].lhs.symbol = "";
		testVals[0].replacedBy = new ArrayList<Symbol>(0);
		
		// 1: E -> T E’
		testVals[1].lhs = ntLookup.get("E");
		testVals[1].replacedBy = new ArrayList<Symbol>
			( Arrays.asList(new Symbol[] { 
				ntLookup.get("T"), ntLookup.get("E’") 
			}));
		
		// 2: E’ -> + T E’
		testVals[2].lhs = ntLookup.get("E’");
		testVals[2].replacedBy = new ArrayList<Symbol>
			( Arrays.asList(new Symbol[] { 
				tkLookup.get("+"), ntLookup.get("T"), ntLookup.get("E’") 
			}));
		
		// 3: E’ -> - T E’
		testVals[3].lhs = ntLookup.get("E’");
		testVals[3].replacedBy = new ArrayList<Symbol>
			( Arrays.asList(new Symbol[] { 
				tkLookup.get("-"), ntLookup.get("T"), ntLookup.get("E’") 
			}));
		
		// 4: E’ ->  𝞴
		// NOTE: lambda RHS should produce empty 
		testVals[4].lhs = ntLookup.get("E’");
		testVals[4].replacedBy = new ArrayList<Symbol>
			( Arrays.asList(new Symbol[] { }));
		
		// 5: T -> P T’
		testVals[5].lhs = ntLookup.get("T");
		testVals[5].replacedBy = new ArrayList<Symbol>
			( Arrays.asList(new Symbol[] { 
				ntLookup.get("P"), ntLookup.get("T’") 
			}));
		
		// 6: T’ -> * P T’
		testVals[6].lhs = ntLookup.get("T’");
		testVals[6].replacedBy = new ArrayList<Symbol>
			( Arrays.asList(new Symbol[] { 
				tkLookup.get("*"), ntLookup.get("P"), ntLookup.get("T’")
			}));
		
		// 7: T’ -> / P T’
		testVals[7].lhs = ntLookup.get("T’");
		testVals[7].replacedBy = new ArrayList<Symbol>
			( Arrays.asList(new Symbol[] { 
				tkLookup.get("/"), ntLookup.get("P"), ntLookup.get("T’")
			}));
		
		// 8: T’ -> 𝞴
		testVals[8].lhs = ntLookup.get("T’");
		testVals[8].replacedBy = new ArrayList<Symbol>
			( Arrays.asList(new Symbol[] { }));
		
		// 9: P -> F P’
		testVals[9].lhs = ntLookup.get("P");
		testVals[9].replacedBy = new ArrayList<Symbol>
			( Arrays.asList(new Symbol[] { 
				ntLookup.get("F"), ntLookup.get("P’") 
			}));
		
		// 10: P’ -> ^ F P’
		testVals[10].lhs = ntLookup.get("P’");
		testVals[10].replacedBy = new ArrayList<Symbol>
			( Arrays.asList(new Symbol[] { 
				tkLookup.get("^"), ntLookup.get("F"), ntLookup.get("P’")
			}));
		
		// 11: P’ -> 𝞴
		testVals[11].lhs = ntLookup.get("P’");
		testVals[11].replacedBy = new ArrayList<Symbol>
			( Arrays.asList(new Symbol[] { }));
		
		// 12: F -> ( E )
		testVals[12].lhs = ntLookup.get("F");
		testVals[12].replacedBy = new ArrayList<Symbol>
			( Arrays.asList(new Symbol[] { 
				tkLookup.get("("), ntLookup.get("E"), tkLookup.get(")")
			}));
		
		// 13: F -> num
		testVals[13].lhs = ntLookup.get("F");
		testVals[13].replacedBy = new ArrayList<Symbol>
			( Arrays.asList(new Symbol[] { 
				tkLookup.get("num")
			}));
		
		return testVals;
	}

	static int[][] getTestParseTable() {
		return new int[][] {
			/* 	  +   -   *   /   (   )   ^  num  $ */
			{ 0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
/* 1 */		{ 0, 98, 98, 98, 98,  1, 99, 98,  1, 99 },
/* 2 */		{ 0,  2,  3, 98, 98, 98,  4, 98, 98, 4  },
/* 3 */		{ 0, 99, 99, 98, 98,  5, 99, 98,  5, 99 },
/* 4 */		{ 0,  8,  8,  6,  7, 98,  8, 98, 98, 8  },
/* 5 */		{ 0, 99, 99, 99, 99,  9, 99, 98,  9, 99 },
/* 6 */		{ 0, 11, 11, 11, 11, 98, 11, 10, 98, 11 },
/* 7 */		{ 0, 99, 99, 99, 99, 12, 99, 99, 13, 99 },
		};
	}

	static int[][] getSimplifiedParseTable() {
		return new int[][] {
			/* 	  +   -   *   /   (   )   ^  num  $ */
			{ 0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
/* 1 */		{ 0,  0,  0,  0,  0,  1,  0,  0,  1,  0 },
/* 2 */		{ 0,  2,  3,  0,  0,  0,  4,  0,  0, 4  },
/* 3 */		{ 0,  0,  0,  0,  0,  5,  0,  0,  5,  0 },
/* 4 */		{ 0,  8,  8,  6,  7,  0,  8,  0,  0, 8  },
/* 5 */		{ 0,  0,  0,  0,  0,  9,  0,  0,  9,  0 },
/* 6 */		{ 0, 11, 11, 11, 11,  0, 11, 10,  0, 11 },
/* 7 */		{ 0,  0,  0,  0,  0, 12,  0,  0, 13,  0 },
		};
	}

	static String getTestParseOutput() {
		return "Fire 1\n" +
				"Fire 5\n" +
				"Fire 9\n" +
				"Fire 13\n" +
				"Match and Pop 3\n" +
				"Fire 11\n" +
				"Fire 8\n" +
				"Fire 2\n" +
				"Match and Pop +\n" +
				"Fire 5\n" +
				"Fire 9\n" +
				"Fire 13\n" +
				"Match and Pop 4\n" +
				"Fire 11\n" +
				"Fire 6\n" +
				"Match and Pop *\n" +
				"Fire 9\n" +
				"Fire 12\n" +
				"Match and Pop (\n" +
				"Fire 1\n" +
				"Fire 5\n" +
				"Fire 9\n" +
				"Fire 13\n" +
				"Match and Pop 5\n" +
				"Fire 11\n" +
				"Fire 7\n" +
				"Match and Pop /\n" +
				"Fire 9\n" +
				"Fire 13\n" +
				"Match and Pop 3\n" +
				"Fire 11\n" +
				"Fire 8\n" +
				"Fire 2\n" +
				"Match and Pop +\n" +
				"Fire 5\n" +
				"Fire 9\n" +
				"Fire 13\n" +
				"Match and Pop 2\n" +
				"Fire 11\n" +
				"Fire 8\n" +
				"Fire 4\n" +
				"Match and Pop )\n" +
				"Fire 11\n" +
				"Fire 8\n" +
				"Fire 2\n" +
				"Match and Pop +\n" +
				"Fire 5\n" +
				"Fire 9\n" +
				"Fire 13\n" +
				"Match and Pop 33\n" +
				"Fire 10\n" +
				"Match and Pop ^\n" +
				"Fire 12\n" +
				"Match and Pop (\n" +
				"Fire 1\n" +
				"Fire 5\n" +
				"Fire 9\n" +
				"Fire 13\n" +
				"Match and Pop 1\n" +
				"Fire 11\n" +
				"Fire 7\n" +
				"Match and Pop /\n" +
				"Fire 9\n" +
				"Fire 13\n" +
				"Match and Pop 2\n" +
				"Fire 11\n" +
				"Fire 8\n" +
				"Fire 4\n" +
				"Match and Pop )\n";
	}
}
