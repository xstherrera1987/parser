import static org.junit.Assert.*;

import java.io.*;

import org.junit.*;

public class IntegrationTests {
	@Test public void testSourceGiven() {
		File fPT = new File("res/ParseTable.csv");
		File fPS = new File("res/PredictSet.csv");
		String[][] pt = null;
		String[][] ps = null;
		
		try {
			pt = CsvReader.getStringTable(fPT);
			ps = CsvReader.getStringTable(fPS);
		} catch (FileNotFoundException ignore) { }
		
		TableInterpreter ti = new TableInterpreter();
		try {
			ti.init(pt, ps);
		} catch (PredictSetException e) {
			String msg = "predict set error" + e.getMessage();
			fail(msg);
		} catch (ParseTableException e) {
			String msg = "parse table error" + e.getMessage();
			fail(msg);
		}
		
		Parser p = new Parser();
		p.init(ti.parseTable, ti.productions, ti.END_TOKEN, 
				ti.ntLookup, ti.tkLookup);

		Writer w = new PrintWriter(System.out);
		try {
			p.parse( new FileReader("input/tokens.lang"), w );
		} catch (ParserException e) {
			fail("Parsing Error: " + e.getMessage() );
		} catch (FileNotFoundException e) {
			fail("File not found");
		} catch (IOException e) {
			fail("IO exception");
		}
	}
	
	@Test public void testUserSource() {
		File fPT = new File("res/ParseTable.csv");
		File fPS = new File("res/PredictSet.csv");
		String[][] pt = null;
		String[][] ps = null;
		
		try {
			pt = CsvReader.getStringTable(fPT);
			ps = CsvReader.getStringTable(fPS);
		} catch (FileNotFoundException ignore) { }
		
		TableInterpreter ti = new TableInterpreter();
		try {
			ti.init(pt, ps);
		} catch (PredictSetException e) {
			String msg = "predict set error" + e.getMessage();
			fail(msg);
		} catch (ParseTableException e) {
			String msg = "parse table error" + e.getMessage();
			fail(msg);
		}
		
		Parser p = new Parser();
		p.init(ti.parseTable, ti.productions, ti.END_TOKEN, 
				ti.ntLookup, ti.tkLookup);
		
		Writer w = new PrintWriter(System.out);
		try {
			p.parse( new FileReader("input/user-tokens.lang"), w );
		} catch (ParserException e) {
			fail("Parsing Error: " + e.getMessage() );
		} catch (FileNotFoundException e) {
			fail("File not found");
		} catch (IOException e) {
			fail("IO exception");
		}
	}
}
