import static org.junit.Assert.*;

import java.io.*;

import org.junit.Test;
public class CsvReaderTest {
	@Test public void testGetColumnHeader() {
		File f = new File("res/TestParseTable.csv");
		String[] expected = TestData.getTestColumnHeader();
		String[] results = null;
		
		try {
			results = CsvReader.getColumnHeader(f);
		} catch (FileNotFoundException ignore) { }
		
		for(int i=0; i<expected.length; i++)
			assertEquals( expected[i], results[i] );
	}
	
	@Test public void testGetRowHeader() {
		File f = new File("res/TestParseTable.csv");
		String[] expected = TestData.getTestRowHeader();
		String[] results = null;
		
		try {
			results = CsvReader.getRowHeader(f);
		} catch (FileNotFoundException ignore) { }
		
		for(int i=0; i<expected.length; i++)
			assertEquals( expected[i], results[i] );
	}

	@Test public void testGetStringTableParseTable() {
		File f = new File("res/TestParseTable.csv");
		String[][] expected = TestData.getTestParseTableRaw();
		String[][] results = null;
		
		try {
			results = CsvReader.getStringTable(f);
		} catch (FileNotFoundException ignore) { }
		
		for(int i=0; i<expected.length; i++)
			for (int j=0; j<expected[0].length; j++)
				assertEquals( expected[i][j], results[i][j] );
	}
	
	@Test public void testGetStringTablePredictSet() {
		File f = new File("res/TestPredictSet.csv");
		String[][] expected = TestData.getTestPredictSet();
		
		String[][] results = null;
		try {
			results = CsvReader.getStringTable(f);
		} catch (FileNotFoundException ignore) { }
		
		for(int i=0; i<expected.length; i++)
			for (int j=0; j<expected[0].length; j++)
				assertEquals( expected[i][j], results[i][j] );
	}
}
