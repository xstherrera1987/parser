import static org.junit.Assert.*;
import org.junit.*;

public class TableInterpreterTest {
	@Test public void testGetTokens() {
		Token[] expected = TestData.getTestTokens();
		
		TableInterpreter ti = new TableInterpreter();
		ti.parseTableRaw = TestData.getTestParseTableRaw();
		Token[] tokens = ti.parseTokens(); 
					
		for(int i=0; i<expected.length; i++)
			assertEquals( "token #" + i, expected[i], tokens[i]);
	}
	
	@Test public void testGetNonTerminals() {
		NonTerminal[] expected = TestData.getTestNonTerminals();
		
		TableInterpreter ti = new TableInterpreter();
		ti.parseTableRaw = TestData.getTestParseTableRaw();
		NonTerminal[] nonTerminals = ti.parseNonTerminals();
		
		for(int i=0; i<expected.length; i++)
			assertEquals( "nonterminal #" + i, expected[i], nonTerminals[i]);
	}

	@Test public void testGetProductions() {
		Production[] expected = TestData.getTestProductions();
		
		TableInterpreter ti = new TableInterpreter();
		ti.predictSetRaw = TestData.getTestPredictSet();
		ti.ntLookup = TestData.getTestNonTerminalLookup();
		ti.tkLookup = TestData.getTestTokenLookup();
		
		Production[] productions = null;
		try {
			productions = ti.parseProductions();
		} catch (PredictSetException e) {
			fail("Parse error in Predict Set. " + e.getMessage() );
		}
		
		for(int i=0; i<expected.length; i++)
			assertEquals( "production #" + i, expected[i], productions[i] );
	}
	
	@Test public void testGetParseTable() {
		int[][] expected = TestData.getSimplifiedParseTable();
		
		TableInterpreter ti = new TableInterpreter();
		ti.parseTableRaw = TestData.getTestParseTableRaw();
		ti.ntLookup = TestData.getTestNonTerminalLookup();
		ti.tkLookup = TestData.getTestTokenLookup();
		ti.productions = TestData.getTestProductions();
		
		int[][] parseTable = null;
		try {
			parseTable = ti.parseParseTable();
		} catch (ParseTableException e) {
			fail("Parse error in Parse Table. " + e.getMessage() );
		}


		for(int i=0; i<expected.length; i++)
			for (int j=0; j<expected[0].length; j++)
				assertEquals( "cell:" + i + "," + j, expected[i][j], parseTable[i][j] );
	}
}
