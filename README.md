﻿# Compiler Construction final parser project

### Top-Down LL(1) Table-Based Parser for language defined by:

	<goal> -> <libtoken><libtoken_tail><start> | <start>

	<libtoken_tail> -> <libtoken><libtoken_tail> | 𝞴

	<libtoken> -> lib

	<start> -> <code>

	<code> -> program id ; <variables> <main> <end>

	<variables> -> var <variable_type> : id ; <variable_type_tail>

	<variable_type_tail> -> <variable_type> : id ; <variable_type_tail> | 𝞴

	<variable_type> -> <variable> <variable_tail>

	<variable_tail> -> , <variable> <variable_tail>  | 𝞴

	<variable> -> id

	<main> -> begin <statement_list> return int ;

	<statement_list> -> <statement> <statement_tail>

	<statement_tail> -> <statement> <statement_tail>  | 𝞴

	<statement> -> 
	
		id := <expression> ; |

		device_open filetoken ;  |
	
		for id := id to id do <statement_list> end do; |

		repeat do <statement_list> until ( <boolean_condition> ) end do; |

		while ( <boolean_condition> ) do <statement_list> end do ; |
	
		if ( <boolean_condition> ) then begin <statement_list> end ; |
	
		else begin <statement_list> end ; |

		device_close filetoken ; |

		read_from_device filetoken ; |

		write_to_device filetoken ; |

		println ( <expression> ) ; |

		read( <variable_type> ) ;

	<boolean_condition> -> <expression> <relational_op> <expression>

	<expression> -> <primary> <primary_tail> | stringliteral

	<primary_tail> -> + <primary> <primary_tail> | - <primary><primary_tail> | 𝞴

	<primary> -> <secondary> <secondary_tail>

	<secondary_tail> -> * <secondary> <secondary_tail> | / <secondary> <secondary_tail> | 𝞴

	<secondary> -> ( <expression> ) | id | int | real | scientific | currencylit | abs( <expression> )

	<end> -> end.


========================

req'd: Java7

optional: junit4+, bash

