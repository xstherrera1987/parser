#!/bin/bash

if [ $# -ne 2 ]; then
	echo "usage: $(basename $0) data.csv arrayName"
	exit 1
fi

echo "String[][] $2 = {"

cat $1 | \
# surround every column with double quotes (doesn't match last) 
sed 's/\([^,]*\),/"\1",/g' | \
# surround last column, add comma for next row
sed 's/,\([^,]*\)$/,"\1"/g' | \
# surround every row with { }
sed 's/.*/{&},/g' | \
# remove comma from last row
sed "$ s/,$//" | \
# add tabs for proper Java style 
sed 's/.*/\t&/'

echo
echo -e "};\n"

