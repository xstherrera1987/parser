public abstract class Symbol {
	int id;
	String symbol;

	abstract boolean isTerminal();
	
	@Override public boolean equals(Object o) {
		// this ensures (anonymous) subclasses will also match
		if ( !this.getClass().isAssignableFrom(o.getClass()) )
			return false;
		
		Symbol sO = (Symbol) o;
		return id == sO.id && symbol.equals(sO.symbol); 
	}
	
	@Override public String toString() {
		return symbol+ "  #" + id;
	}
	
	// TODO i think lambda should be terminal because it is always a leaf
	// it is not a token though (?)
	static final Symbol LAMBDA = new Symbol() {
		@Override boolean isTerminal() { return true; }
	};
	static {
		LAMBDA.symbol = "𝞴";
	}
}
