public class Token extends Symbol {
	@Override public boolean isTerminal() {
		return true;
	}
	
	// Null Token
	public static final Token NULL_TOKEN = new Token() {
		@Override public boolean equals(Object o) {
			if (this == o) return true;
			
			if ( !(o instanceof Token) )
				return false;
			Token sO = (Token) o;
			
			return NULL_TOKEN.id == sO.id && 
					NULL_TOKEN.symbol.equals(sO.symbol);
		}
	};
	
	static {
		NULL_TOKEN.id = 0;
		NULL_TOKEN.symbol = "";
	}
}
