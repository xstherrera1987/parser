import java.util.*;

public class TableInterpreter {
	Token END_TOKEN = new Token();
	
	boolean ready = false;
	Production[] productions;
	int[][] parseTable;
	
	Token[] tokens;
	NonTerminal[] nonTerminals;
	Map<String, NonTerminal> ntLookup;
	Map<String, Token> tkLookup;
	
	String[][] predictSetRaw;
	String[][] parseTableRaw;
	
	public static final int ERROR = 0;
	public static final int SCAN = 98;
	public static final String SCAN_RAW ="98";
	public static final int POP = 99;
	public static final String POP_RAW = "99";
	
	public void init(String[][] parseTableRaw, String[][] predictSetRaw) 
	throws PredictSetException, ParseTableException {
		this.parseTableRaw = parseTableRaw;
		this.predictSetRaw = predictSetRaw;
		
		tokens = parseTokens();
		int lastCol = tokens.length-1;
		END_TOKEN.symbol = this.tokens[lastCol].symbol;
		END_TOKEN.id = lastCol;
		
		nonTerminals = parseNonTerminals();
		ntLookup = nonTerminalLookup( this.nonTerminals );
		tkLookup = tokenLookup( this.tokens );
		
		productions = parseProductions();
		parseTable = parseParseTable();
		
		ready = true;
	}
	
	// tokens are in the parse table column header
	Token[] parseTokens() {
		int numTokens = parseTableRaw[0].length;
		
		Token[] tokens = new Token[numTokens];
		Token token;
		
		// NOTE: the first entry is empty (start from 1)
		tokens[0] = Token.NULL_TOKEN;
		
		String cell;
		for (int i=1; i<numTokens; i++) {
			token = new Token();
			token.id = i;
			cell = parseTableRaw[0][i].trim();
			if ( "comma".equals(cell) ) token.symbol = ",";
			else token.symbol = cell;
			tokens[i] = token;
		}
		
		return tokens;
	}
	
	NonTerminal[] parseNonTerminals() {
		int numNonTerminals = parseTableRaw.length;
		
		NonTerminal[] nonTerminals = new NonTerminal[numNonTerminals];
		NonTerminal nonTerminal;
		
		// NOTE: the first entry is empty (start from 1)
		nonTerminals[0] = NonTerminal.NULL_NONTERMINAL;
		
		for (int i=1; i<numNonTerminals; i++) {
			nonTerminal = new NonTerminal();
			nonTerminal.id = i;
			nonTerminal.symbol = parseTableRaw[i][0].trim();
			nonTerminals[i] = nonTerminal;
		}
		
		return nonTerminals;
	}
	
	Production[] parseProductions() throws PredictSetException {
		int numProds = predictSetRaw.length;
		Production[] prods = new Production[ numProds ];
		
		prods[0] = Production.NULL_PRODUCTION;
		for (int i=1; i<numProds; i++) {
			prods[i] = new Production();
			prods[i].id = i;
		}
		
		String prodRaw;
		String[] prodSymbolsRaw;
		String symbolRaw;
		List<Symbol> rhs;
		Symbol symbol;
		NonTerminal lhs;
		for (int i=1; i<numProds; i++) {
			prodRaw = predictSetRaw[i][1].trim();
			prodSymbolsRaw = prodRaw.split(" ");
			
			// the first token is the lhs of production
			lhs = ntLookup.get( prodSymbolsRaw[0].trim() );
			if (null == lhs)
				throw new PredictSetException("Symbol " + prodSymbolsRaw[0] + 
						" unparsable. Row: " + i);
			prods[i].lhs = lhs;
			
			// prodSymbolsRaw[1] is the "->"
			
			// the remaining "replacedBy" symbols, eg: the RHS of a production
			rhs = new ArrayList<Symbol>();
			for (int j=2; j<prodSymbolsRaw.length; j++) {
				symbolRaw = prodSymbolsRaw[j].trim();
				
				if ("".equals(symbolRaw) ) continue;
				if (NonTerminal.LAMBDA.symbol.equals(symbolRaw)) break;
				
				if ("comma".equals(symbolRaw)) symbolRaw = ",";

				// check if terminal/non-terminal
				symbol = null;
				if ( null != (symbol = ntLookup.get(symbolRaw)) );
				else if (null != (symbol = tkLookup.get(symbolRaw)) );
				else if (null == symbol)
					throw new PredictSetException("Production " + symbolRaw + 
							" unparsable. Row: " + i);
				
				rhs.add(symbol);
			}
			
			prods[i].replacedBy = rhs;
		}
		
		return prods;
	}
	
	int[][] parseParseTable() throws ParseTableException {
		int rows = parseTableRaw.length;
		int cols = parseTableRaw[0].length;
		
		int[][] retVal = new int[rows][cols];
		String cellRaw = null;
		int cell = 0;
		for (int i=0; i<rows; i++) {
			for (int j=0; j<cols; j++) {
				// ignore column/row headers
				if (0 == j || 0 == i) {
					retVal[i][j] = 0;
				} else {
					cellRaw = parseTableRaw[i][j].trim();
					
					switch (cellRaw) {
					case "err":
					case "error":
					case "":
						cell = 0;
						break;
					case "pop":
					case POP_RAW:
						cell = POP;
						break;
					case "scan":
					case SCAN_RAW:
						cell = SCAN;
						break;
					default:
						try {
							cell = Integer.parseInt(cellRaw);
						} catch ( NumberFormatException ignore) { 
							throw new ParseTableException
							("Can not parse: " + cellRaw + " @ " + i + "," + j);
						}
					}
					
					retVal[i][j] = cell;
				}
			}
		}
		
		return retVal;
	}

	Map<String, Token> tokenLookup(Token[] tokens) {
		Map<String, Token> lookup = new HashMap<String, Token>();
		for (Token t: tokens )
			lookup.put(t.symbol, t);
		
		return lookup;
	}
	
	Map<String, NonTerminal> nonTerminalLookup(NonTerminal[] nonTerminals) {
		Map<String, NonTerminal> lookup = new HashMap<String, NonTerminal>();
		for (NonTerminal n: nonTerminals )
			lookup.put(n.symbol, n);
		
		return lookup;
	}
}
