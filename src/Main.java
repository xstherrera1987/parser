import java.io.*;

public class Main {
	static final String DEFAULT_PARSE_TABLE = "res/ParseTable.csv";
	static final String DEFAULT_PREDICT_SET = "res/PredictSet.csv";
	static final String DEFAULT_SOURCE_PROGRAM = "input/tokens.lang";
	
	public static void main(String[] args) throws Exception {
		File parseTableFile = null, predictSetFile = null, sourceFile = null;
		if (args.length == 0) {
			parseTableFile = openFileOrDie(DEFAULT_PARSE_TABLE);
			predictSetFile = openFileOrDie(DEFAULT_PREDICT_SET);
			sourceFile = openFileOrDie(DEFAULT_SOURCE_PROGRAM);
		} else if (args.length < 3) {
			System.out.println( usage() );	
			System.exit(1);
		} else {
			parseTableFile = openFileOrDie( args[0] );
			predictSetFile = openFileOrDie( args[1] );
			sourceFile = openFileOrDie( args[2] );
		}

		String[][] parseTable = CsvReader.getStringTable(parseTableFile);
		String[][] predictSet = CsvReader.getStringTable(predictSetFile);
		TableInterpreter ti = new TableInterpreter();
		ti.init(parseTable, predictSet);
		
		Parser p = new Parser();
		p.init(ti.parseTable, ti.productions, ti.END_TOKEN, 
				ti.ntLookup, ti.tkLookup);
		
		Writer w = new PrintWriter(System.out);
		try {
			p.parse( new FileReader(sourceFile), w );
		} catch (ParserException e) {
			System.out.println("Parsing Error. Exiting.");
		}
	}
	
	static File openFileOrDie(String filename) {
		File f = new File(filename);
		if ( !f.canRead() ) {
			System.out.println("Fatal error: could not open " + filename);
			System.exit(1);
		}
		
		return f;
	}

	static String usage() {
		return "usage:\nParser parseTable.csv predictSet.csv sourceCode.lang";
	}
}
