import java.util.*;
public class Production {
	NonTerminal lhs;
	int id;
	// load these in regular readable order, the Parser will reverse them
	List<Symbol> replacedBy;
	
	// Null-Safe
	@Override public String toString() {
		StringBuffer sb = new StringBuffer();
		
		if (null != lhs && null != lhs.symbol)
			sb.append(lhs.symbol);
		else
			sb.append("NullLhs");
		
		sb.append(" -> ");
		
		if (null != replacedBy) {
			for(int i=0; i<replacedBy.size(); i++) {
				sb.append( replacedBy.get(i).symbol );
				sb.append( " " );
			}
		} else {
			sb.append("NullRhs");
		}
		
		return sb.toString();
	}
	
	@Override public boolean equals(Object o) {
		if (null == o)
			return false;
		
		if ( !(o instanceof Production) )
			return false;
		
		Production pO = (Production) o;
		
		if ( id != pO.id ) 
			return false;
		
		if ( !lhs.equals(pO.lhs) )
			return false;
		
		if (replacedBy.equals(pO.replacedBy))
			return true;
		
		return false;
	}
	
	// Null Production
	public static final Production NULL_PRODUCTION = new Production() {
		// TODO is this good design?
		// Null Production should only equal itself
		@Override public boolean equals(Object o) { return this == o; }
		@Override public String toString() { return "NullProduction"; }
	};
	static {
		NULL_PRODUCTION.id = 0;
		NULL_PRODUCTION.lhs = NonTerminal.NULL_NONTERMINAL;
		NULL_PRODUCTION.replacedBy = new ArrayList<Symbol>(0);
	}
}
