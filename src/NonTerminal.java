public class NonTerminal extends Symbol {
	@Override public boolean isTerminal() {
		return false;
	}
	
	// Null NonTerminal
	public static final NonTerminal NULL_NONTERMINAL = new NonTerminal() {
		@Override public boolean equals(Object o) {
			if (this == o) return true;
			
			if ( !(o instanceof NonTerminal) )
				return false;
			NonTerminal sO = (NonTerminal) o;
			
			return NULL_NONTERMINAL.id == sO.id && 
					NULL_NONTERMINAL.symbol.equals(sO.symbol);
		}
	};
	static {
		NULL_NONTERMINAL.id = 0;
		NULL_NONTERMINAL.symbol = "";
	}
}
