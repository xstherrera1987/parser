import java.io.*;
import java.util.*;

public class Parser {
	int[][] parseTable;
	Map<String, NonTerminal> ntLookup;
	Map<String, Token> tkLookup;
	Production[] productions;
	Token END_TOKEN;
	
	String currentRead;
	Symbol currentToken;
	int tokenId, prodId;
	
	Stack<Symbol> s;
	Symbol stackTop;
	Production prod;

	Writer wr;
	Scanner source;
	
	void init (int[][] parseTable, Production[] prods, Token end,
	Map<String, NonTerminal> ntLookup, Map<String, Token> tkLookup) {
		this.END_TOKEN = end;
		this.parseTable = parseTable;
		this.productions = prods;
		this.ntLookup = ntLookup;
		this.tkLookup = tkLookup;
	}
	
	public void parse(Reader is, Writer wr) throws ParserException, 
	IOException {
		source = new Scanner(is);
		this.wr = wr;
		s = new Stack<Symbol>();

		// push goal, start the parse
		s.push( productions[1].lhs );

		// first token from file
		currentRead = source.next().trim();
		
		while ( source.hasNext() ) {
			currentToken = null;
			if ( null != (currentToken = ntLookup.get(currentRead)) );
			else if ( null != (currentToken = tkLookup.get(currentRead)) );
			else currentToken = END_TOKEN;
			
			tokenId = currentToken.id;

			// get next table entry
			stackTop = s.peek();
			
			// push production corresponding to it from table onto the stack
			if ( !stackTop.isTerminal() ) {
				prodId = parseTable[stackTop.id][tokenId];

				if (0 < prodId && productions.length > prodId) {
					prod = productions[prodId];
					wr.append("Fire " + prodId +"\n");					
					s.pop();
					
					// push onto the stack in reverse
					for (int i=prod.replacedBy.size()-1; i>=0; i--)
						s.push(prod.replacedBy.get(i));
					
				} else if ( TableInterpreter.POP == prodId ) {
					wr.append("Pop Error\n");
				} else if ( TableInterpreter.SCAN == prodId ) {
					wr.append("Scan Error\n");
				} else {
					wr.append("Error\n");
				}
			} else if ( stackTop.symbol.equals( currentToken.symbol ) ) {
			// if top of stack matches token read,
			// then pop token from stack and remove token from stream
				wr.append("Match and Pop " + currentRead + "\n");
				s.pop();

				currentRead = source.next().trim();
				if (!source.hasNext()) {
					break;
				}
			} else {
				wr.append("error\n");
			}
			wr.flush();
		}

		source.close();
	}
}
