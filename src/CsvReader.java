import java.io.*;
import java.util.*;

// TODO eliminate use of scanner, just split on \n
// TODO allow use of comma literal within double quotes
public class CsvReader {

	static int countCommas(String s) {
		int count = 0;
		for (int i=0, len=s.length(); i<len; i++)
			if ( s.charAt(i) == ',' ) count++;
		return count;
	}
	
	static String[] getColumnHeader(File f) throws FileNotFoundException {
		Scanner s = new Scanner(f);
		
		// first and only line
		String line = s.nextLine();
		s.close();
		
		int numColumns = countCommas(line) + 1;
		String []columns = line.split(",", numColumns);

		return columns;
	}

	static String[] getRowHeader(File f) throws FileNotFoundException {
		ArrayList<String> data = new ArrayList<String>();
		Scanner s = new Scanner(f);
		String line, token;
		
		// need first line to calculate column count
		line = s.nextLine();
		final int numColumns = countCommas(line) + 1;
		while ( true ) {
			// first column only
			token = line.split(",", numColumns)[0];
			data.add(token);
			
			if ( s.hasNext() )
				line = s.nextLine();
			else
				break;
		}
		s.close();

		// pass new String[] to avoid casting problems
		return data.toArray( new String[data.size()] );
	}

	static String[][] getStringTable(File f) throws FileNotFoundException {
		Scanner s = new Scanner(f);
		// column count is constant (calculated from first line)
		// but the row count is unknown until full file is read
		ArrayList<String[]> data = new ArrayList<String[]>();
		String[] row;
		String line;
		
		
		// need first line to calculate column count
		line = s.nextLine();
		final int numColumns = countCommas(line) + 1;
		while ( true ) {
			row = line.split(",", numColumns);
			data.add(row);
			
			if ( s.hasNext() )
				line = s.nextLine();
			else
				break;
		}
		s.close();
		
		final int rows = data.size(); 
		String[][] retVal = new String[rows][numColumns];
		for (int i=0; i<rows; i++) {
			retVal[i] = data.get(i);
		}
		
		return retVal;
	}
}
